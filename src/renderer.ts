import { ipcRenderer } from "electron";
import { IInit } from "./IMessages";

ipcRenderer.send("init", null);
ipcRenderer.on("setSDKToolsStatusLabel", (event: Event, arg: IInit) => {
    const SDKToolsStatusLabel = document.getElementById("SDKToolsStatusLabel");

    if (arg.SDKManagerInstalled) {
        SDKToolsStatusLabel.textContent = "Found. Version: " + arg.Version;
        SDKToolsStatusLabel.style.color = "green";
    }
    else SDKToolsStatusLabel.style.color = "red";
});
