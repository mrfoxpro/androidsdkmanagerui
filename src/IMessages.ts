export interface IInit {
    SDKManagerInstalled: boolean;
    Path?: string;
    Version: string;
}
