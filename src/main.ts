import { app, BrowserWindow, Event, ipcMain } from "electron";
import * as path from "path";
import { IInit } from "./IMessages";
import SDKManagerHandler from "./SDKManager/sdkManagerHandler";
let mainWindow: Electron.BrowserWindow;

function createWindow() {
  mainWindow = new BrowserWindow({
    height: 600,
    width: 800,
  });
  mainWindow.loadFile(path.join(__dirname, "../index.html"));

  // mainWindow.webContents.openDevTools();

  mainWindow.on("closed", () => {
    mainWindow = null;
  });


}
ipcMain.on("init", (event: Event) => {
  const res: IInit = {
    SDKManagerInstalled: SDKManagerHandler.checkIfSDKManagerInstalled(),
    Version: SDKManagerHandler.getVersion()
  };
  event.sender.send("setSDKToolsStatusLabel", res);
});
app.on("ready", createWindow);

app.on("window-all-closed", () => {
  /* On OS X it is common for applications and their menu bar
  to stay active until the user quits explicitly with Cmd + Q */
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  /* On OS X it"s common to re-create a window in the app when the
   dock icon is clicked and there are no other windows open. */
  if (mainWindow === null) {
    createWindow();
  }
});