import { exec, execSync } from "child_process";
export default class SDKManagerHandler {
    private static Version: string;
    public static checkIfSDKManagerInstalled(): boolean {
        const out = execSync("sdkmanager --version").toString().trim();
        if (out.length < 7) {
            SDKManagerHandler.Version = out;
            return true;
        }
        return false;
    }
    public static getVersion(): string {
        return this.Version;
    }
}


